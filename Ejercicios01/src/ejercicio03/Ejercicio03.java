package ejercicio03;

public class Ejercicio03 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int num;
		num = 11;
		
		//Incrementar en 77
		num = num + 77;
		
		//Misma operacion con operador incremental
		//num += 77;
		
		System.out.println(num);
		
		//Decrementar en 3
		num = num -3;
		//Otra forma
		//num -= 3;
		
		System.out.println(num);
		
		//Duplicar su valor 
		num *= 2; 
		//es lo mismo que -> num = num * 2
		System.out.println(num);
	}

}
