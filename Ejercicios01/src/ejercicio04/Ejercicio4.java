package ejercicio04;

public class Ejercicio4 {

	public static void main(String[] args) {
			int varA = 6;
			int varB = -45;
			int varC = 500;
			int varD = -1234;
			
			//Muestro el valor de las 4 variables
			System.out.println(varA);
			System.out.println(varB);
			System.out.println(varC);
			System.out.println(varD);
			
			//Para intercambiar el valor entre diferentes variables
			//Sin perder ningun valor, necesito como 
			//m�nimo tener una variable mas
			
			int aux = varA;
			//Ahora ya he guardado el primer valor
			//Al asignar otro valor a varA, piso el valor que tenia anteriormente
			varA = varB;
			varB = varC;
			varC = varD;
			//Y como varA ya no contiene su valor original
			//Hago uso de la variable en la que lo guarde
			varD = aux;
			
			System.out.println(varA);
			System.out.println(varB);
			System.out.println(varC);
			System.out.println(varD);
			

	}

}
