package ejercicio05;

public class Ejercicio05 {

	public static void main(String[] args) {

		int numeroA = 5;
		int numeroB = 3;
		int numeroC = -12;
		
		boolean expresion;
		
		expresion = numeroA > 3;
		System.out.println(expresion);
		
		expresion = numeroA > numeroC;
		System.out.println(expresion);
		
		expresion = numeroA < numeroC;
		System.out.println(expresion);
		
		expresion = numeroB < numeroC;
		System.out.println(expresion);
		
		expresion = numeroB != numeroC;
		System.out.println(expresion);
		
		expresion = numeroA == 3;
		System.out.println(expresion);
		
		expresion = numeroA * numeroB == 15;
		System.out.println(expresion);
		
		expresion = numeroC / numeroB == 4;
		System.out.println(expresion);
		
		expresion = numeroC / numeroB < numeroA;
		System.out.println(expresion);
		
		expresion = (numeroA * 45) > 3;
		System.out.println(expresion);
		
		expresion = numeroC / numeroB < -10;
		System.out.println(expresion);
		
		expresion = numeroA % numeroB == 2;
		System.out.println(expresion);
		
		expresion = numeroA + numeroB + numeroC == 5;
		System.out.println(expresion);
		
		expresion = (numeroA + numeroB == 8) && (numeroA - numeroB == 2);
		System.out.println(expresion);
		
		expresion = (numeroA + numeroB == 8) || (numeroA - numeroB == 6);
		System.out.println(expresion);
		
		expresion = numeroA > 3 && numeroB > 3 && numeroC < 3;
		System.out.println(expresion);
		
		expresion = numeroA > 3 && numeroB >= 3 && numeroC < -3;
		System.out.println(expresion);
		

	}

}
