package ejercicio11;

public class Ejercicio11 {

	public static void main(String[] args) {
		
		long maximo = 9223372036854775807L;
		long minimo = -9223372036854775808L;
		
		System.out.println(maximo);
		System.out.println(minimo);
		
		short short1 = (short)maximo;
		short short2 = (short)minimo;
		
		System.out.println(short1);
		System.out.println(short2);
		
		System.out.println("Al convertir de un tipo long a un tipo short, "
				+ "estoy pasando de 8 bytes a 2 bytes. Pierdo  6 bytes de datos,"
				+ "por lo que el numero final no ser� el mismo que el inicial");
		
	}
}
