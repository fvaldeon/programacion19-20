package ejercicio18;

import java.io.File;
import java.time.LocalDate;
import java.util.Scanner;

public class Ejercicio18 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce un byte");
		byte numByte = input.nextByte();
		System.out.println("El byte introducido es: " + numByte);
		
		System.out.println("Introduce un short");
		short numShort = input.nextShort();
		System.out.println("El short introducido es: " + numShort);
		
		System.out.println("Introduce un int");
		int numInt = input.nextInt();
		System.out.println("El int introducido es: " + numInt);
		
		System.out.println("Introduce un long");
		long numLong = input.nextLong();
		System.out.println("El long introducido es: " + numLong);
		
		System.out.println("Introduce un float");
		float numFloat = input.nextFloat();
		System.out.println("El float introducido es: " + numFloat);
		
		System.out.println("Introduce un double");
		double numDouble = input.nextDouble();
		System.out.println("El double introducido es: " + numDouble);
		
		//Limpio buffer de lectura antes de leer un String
		input.nextLine();
		
		System.out.println("Introduce un char");
		char caracter = input.nextLine().charAt(0);
		System.out.println("El char introducido es: " + caracter);
		
		input.close();
	}

}
