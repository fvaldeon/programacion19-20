package ejercicio05;

import java.util.Scanner;

public class Ejercicio5 {

	public static void main(String[] args) {

		Scanner escaner = new Scanner(System.in);
		
		System.out.println("Introduce una cadena");
		String cadena1 = escaner.nextLine();
		
		System.out.println("Introduce otra cadena");
		String cadena2 = escaner.nextLine();
		
		System.out.println(cadena1.equals(cadena2) ? "Las cadenas son iguales" : "Las cadenas no son iguales");
		
		escaner.close();
	}

}
