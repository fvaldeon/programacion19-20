package ejercicio08;

import java.util.Scanner;

public class Ejercicio8 {

	public static void main(String[] args) {
		
		Scanner escaner = new Scanner(System.in);
		
		System.out.println("Introduce un numero natural (5 cifras)");
		int numero = escaner.nextInt();
		
		System.out.println(numero / 10000);
		System.out.println(numero / 1000);
		System.out.println(numero / 100);
		System.out.println(numero / 10);
		System.out.println(numero);
		
		
		System.out.println("Introduce de nuevo un numero de 5 cifras");
		String cadena = escaner.nextLine();
		
		System.out.println(cadena);
		
		System.out.println(cadena);
		
		
		
		
		
		
		
		
		
		
		
		escaner.close();
	}

}
