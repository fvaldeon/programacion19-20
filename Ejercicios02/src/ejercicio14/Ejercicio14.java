package ejercicio14;

import java.util.Scanner;

public class Ejercicio14 {

	public static void main(String[] args) {
		Scanner escaner = new Scanner(System.in);
		
		System.out.println("Introduce una cadena");
		String cadena1 = escaner.nextLine();

		System.out.println("¿Cuantos caracteres quieres mostrar?");
		int cantidad = escaner.nextInt();
		
		System.out.println(cadena1.substring(0, cantidad));
		
		escaner.close();
	}

}
