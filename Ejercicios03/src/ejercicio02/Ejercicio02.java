package ejercicio02;

import java.util.Scanner;

public class Ejercicio02 {

	public static void main(String[] args) {
		
		Scanner escaner =  new Scanner(System.in);
		
		System.out.println("Introduce un numero entero");
		int numero = escaner.nextInt();
		
		if(numero % 10 == 0) {
			System.out.println("Es multiplo de 10");
		} else {
			System.out.println("No es multiplo de 10");
		}
				
		escaner.close();
		
		
		
		
	}

}
