package ejercicio05;

import java.util.Scanner;

public class Ejercicio05 {

	public static void main(String[] args) {

		Scanner lector = new Scanner(System.in);

		System.out.println("Introduce un mes escrito");
		String mes = lector.nextLine();

		// Lo convierto a minusculas y me aseguro
		// que coincide con el valor de los casos
		mes = mes.toLowerCase();

		switch (mes) {
		case "enero":
		case "marzo":
		case "mayo":
		case "julio":
		case "agosto":
		case "octubre":
		case "diciembre":
			System.out.println("Tiene 31 dias");
			break;

		case "febrero":
			System.out.println("Tiene 28 dias");
			break;

		case "abril":
		case "junio":
		case "septiembre":
		case "noviembre":
			System.out.println("Tiene 30 dias");
			break;

		default:
			System.out.println("Opcion no contemplada");
		}

		lector.close();
	}

}
