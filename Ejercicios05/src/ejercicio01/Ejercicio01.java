package ejercicio01;

import java.util.Scanner;

public class Ejercicio01 {

	static final String MENSAJE_FINAL = "Fin del programa";
	
	public static void main(String[] args) {
		Scanner lectorTeclado = new Scanner(System.in);
		
		System.out.println("Introduce un numero");
		int numero = lectorTeclado.nextInt();
		
		for(int i = 1 ; i <= numero ; i++) {
			System.out.println(i);
		}
		
		for(int i = 1 ; i <= numero ; i++) {
			System.out.print(i + " ");
		}
		
		System.out.println(MENSAJE_FINAL);
		
		lectorTeclado.close();
	}
}
