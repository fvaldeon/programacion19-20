package ejercicio05;

import java.util.Scanner;

public class Ejercicio5 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce valor de inicio");
		int inicio = input.nextInt();
		
		System.out.println("Introduce valor de fin");
		int fin = input.nextInt();
		
		System.out.println("Introduce el salto");
		int salto = input.nextInt();
		
		if(salto <= 0) {
			System.out.println("El salto debe ser positivo");
		} else {
			//Si el salto es positivo compruebo el orden de la serie
			//Ascendente o descendente
			if(inicio <= fin) {
				//Orden ascendente
				for(int i = inicio; i <= fin; i = i + salto) {
					System.out.print(i + " ");
				}
			} else {
				//Si inicio no es menor que fin
				//Orden descendente
				for(int i = inicio; i >= fin; i-=salto) {
					System.out.print(i + " ");
				}
			}
		}
		
		input.close();
	}

}
