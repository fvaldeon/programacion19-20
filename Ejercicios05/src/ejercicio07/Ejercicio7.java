package ejercicio07;

import java.util.Scanner;

public class Ejercicio7 {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce la tabla que quieres mostrar");
		int numero = input.nextInt();
		
		for (int i = 1; i <= 10; i++) {
			System.out.println(numero + " x " + i + " = " + (numero * i));
		}
		
		input.close();
	}

}
