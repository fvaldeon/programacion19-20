package ejercicio10;

import java.util.Scanner;

public class Ejercicio10 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		float suma = 0;
		for(int i = 0; i < 5; i++) {
			System.out.println("Introduce un numero real");
			float real = input.nextFloat();
			suma = suma + real;
		}
		
		System.out.println("La suma total es: " + suma);
		
		
		System.out.println(Math.ceil(12.345));
		
		input.close();
	}

}
