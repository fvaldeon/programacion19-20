package ejercicio11;

import java.util.Scanner;

public class Ejercicio11 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce una password");
		String password = input.nextLine();
		 
		for(int i = 0; i < password.length(); i++) {
			
			char caracter = password.charAt(i);
			System.out.print(caracter + " ");
			
		}
		
		input.close();
		
		System.out.println(Math.pow(2.0, 3.0));

	}

}
