package ejercicio16;

import java.util.Scanner;

public class Ejercicio16 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		boolean hayNegativos = false;
		
		for(int i = 0; i < 10; i++){
			System.out.println("Introduce un numero");
			int numero = input.nextInt();
			
			//Revisar si es negativo
			if(numero < 0 ){
				hayNegativos = true;
			}
			
		}
		
		//Comprobar si ha habido negativos
		if( hayNegativos  ){
			System.out.println("Hay negativos");
		}else{
			System.out.println("No hay negativos");
		}
		
		input.close();
	}

}
