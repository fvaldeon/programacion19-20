package ejercicio17;

import java.util.Scanner;

public class Ejercicio17 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce el tama�o del lado");
		int lado = input.nextInt();
		
		for(int i = 0; i < lado; i++ ){
			
			for(int j = 0; j < lado; j++){
				System.out.print("* ");
			}
			System.out.println();
		}
		
		
		input.close();
	}

}
