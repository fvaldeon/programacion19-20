package ejercicio18;

import java.util.Scanner;

public class Ejercicio18 {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce el tamano del deposito");
		float volumenTotal = input.nextFloat();
		
		float litrosActuales = 0;
		
		do{
			
			//Pido litros 
			System.out.println("Introduce los litros (positivo echo, negativo extraigo)");
			litrosActuales = litrosActuales + input.nextFloat();
			
			if(litrosActuales < volumenTotal){
				System.out.println("Faltan " + (volumenTotal - litrosActuales) + " litros");
			}
			
		}while(litrosActuales < volumenTotal);
		
		System.out.println("Se ha llenado el deposito");
		
		input.close();
	}

}
