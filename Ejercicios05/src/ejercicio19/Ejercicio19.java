package ejercicio19;

import java.util.Scanner;

public class Ejercicio19 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		double sumaNotas = 0;
		double nota;
		int cantidadNotas = 0;
		//Para calcular la media necesito contar la cantidad de notas
		//Y sumar el total de las notas
		//Entonces realizaré una división, siempre que la cantidad de notas no sea 0
		
		do{
			System.out.println("Introduce nota del alumno");
			nota = input.nextDouble();
			
			if(nota >= 0){
				sumaNotas = sumaNotas + nota;
				cantidadNotas++;
			}
			
		}while(nota >= 0);
		
		if(cantidadNotas != 0){
			System.out.println("La media es: " + (sumaNotas/cantidadNotas));
		}
		
		input.close();
	}

}
