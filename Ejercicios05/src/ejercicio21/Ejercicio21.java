package ejercicio21;

import java.util.Scanner;

public class Ejercicio21 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		char respuesta;
		
		do{
			
			System.out.println("introduce grados centigrados");
			double centigrados = input.nextDouble();
			input.nextLine();
			
			System.out.println("Kelvin: " + (centigrados + 273));
			
			System.out.println("�Repito (S)?");
			respuesta = input.nextLine().charAt(0);
			
		}while(respuesta == 'S' || respuesta == 's');
		
		input.close();
	}

}
