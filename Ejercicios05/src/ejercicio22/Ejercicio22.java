package ejercicio22;

import java.util.Scanner;

public class Ejercicio22 {

	public static void main(String[] args) {
		//Obtener un numero entre 1 y 100
		int aleatorio =(int) Math.round((Math.random() * 100)) ;
		
		int numero = 0;
		Scanner input = new Scanner(System.in);
				
		do{
			System.out.println("Adivina el numero");
			numero = input.nextInt();
			
			if(numero > aleatorio){
				System.out.println("El numero que has introducido es mayor");
			}else if(numero < aleatorio){
				System.out.println("El numero que has introducido es menor");
			}else{
				System.out.println("Has acertado");
			}
			
		}while(numero != aleatorio);
			
		input.close();
	}

}
