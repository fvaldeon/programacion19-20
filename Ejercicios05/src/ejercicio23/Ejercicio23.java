package ejercicio23;

import java.util.Scanner;

public class Ejercicio23 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Introduce un numero binario (8 cifras)");
		int binario = input.nextInt();
		
		int resultado = 0;
		
		//Voy multiplicando cada cifra binaria por potencias de 2
		// 11010101
		for(int i = 0; i < 8; i++){
			//Cojo el bit de menos peso
			int bit = binario % 10;
			//Elimino ese bit
			binario = binario / 10;
			
			resultado = resultado + (int)( Math.pow(2, i) *  bit);
		}
		
		System.out.println("la conversion es: " + resultado);
		
		input.close();
	}

}
