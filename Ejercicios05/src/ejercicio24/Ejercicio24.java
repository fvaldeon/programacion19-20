package ejercicio24;

import java.util.Scanner;

public class Ejercicio24 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		String cadena;
		
		int contadorVocales = 0;
		int contadorTotalCaracteres = 0;
		String cadenaTotal = "";
		
		do{
			System.out.println("Introduce una cadena");
			cadena = input.nextLine().toLowerCase();
			
			//Voy sumando la cantidad de caracteres de cada cadena
			contadorTotalCaracteres = contadorTotalCaracteres + cadena.length();
			
			//Recorro cada cadena leida en busca de vocales
			for(int i = 0; i < cadena.length(); i++){
				char letra = cadena.charAt(i);
				
				if(letra == 'a' || letra == 'e' || letra == 'i'
				|| letra == 'o' || letra == 'u'){
					contadorVocales++;
				}
			}
			
			//Concateno cada cadena leida
			cadenaTotal = cadenaTotal + cadena;
			
		}while(!cadena.equals("fin"));
		
		System.out.println("cantidad vocales: " + contadorVocales);
		System.out.println("porcentaje de vocales: " + ((double)contadorVocales / contadorTotalCaracteres * 100) + " %");
		System.out.println("Cadena total: \n" + cadenaTotal);
		
		input.close();
	}

}
