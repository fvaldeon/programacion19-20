package ejercicio25;

import java.util.Scanner;

public class Ejercicio25 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		int numeroLeido;
		int contador = 0;
		int maximo = 0;
		int minimo = 0;
		do {
			System.out.println("Introduce numero");
			numeroLeido = input.nextInt();
			
			//El primer numero leido es el mayor y el menor
			if(contador == 0) {
				maximo = numeroLeido;
				minimo = numeroLeido;
			}
			
			//Obtengo el mayor
			if(numeroLeido > maximo) {
				maximo = numeroLeido;
			}
			
			//Obtengo el menor
			if(numeroLeido < minimo) {
				minimo = numeroLeido;
			}
			
			//Cuento los numeros sin contar el 0
			if(numeroLeido != 0) {
				contador++;
			}
			
			
		}while(numeroLeido != 0);
		
		System.out.println("Mayor: " + maximo);
		System.out.println("Menor: " + minimo);
		System.out.println("Cantidad de numeros leidos: " + contador);
		
		input.close();
	}

}
