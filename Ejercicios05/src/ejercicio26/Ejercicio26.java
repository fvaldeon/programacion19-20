package ejercicio26;

import java.util.Scanner;

public class Ejercicio26 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce un n�mero");
		int numero = input.nextInt();
		
		//Para calcular si es perfecto, debo ir sumando sus divisores
		int sumaDivisores = 0;
		
		for(int i = 1; i < numero; i++){
			if(numero % i == 0){
				//Es un divisor, lo guardo y sumo a los anteriores
				sumaDivisores = sumaDivisores + i;
			}
		}
		
		//Si la suma de los divisores es igual al numero
		if(sumaDivisores == numero){
			System.out.println("Es perfecto");
		}else{
			System.out.println("No es perfecto");
		}
		
		input.close();
	}

}
