package ejercicio32;

import java.util.Scanner;

public class Ejercicio32 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		//inicializo los contadores
		int contadorCeros = 0;
		int contadorNegativos = 0;
		int sumaNegativos = 0;
		int contadorPositivos = 0;
		int sumaPositivos = 0;
		
		for(int i = 0; i < 10 ; i++) {
			System.out.println("Introduce un numero");
			int numero = input.nextInt();
			
			if(numero == 0) {
				contadorCeros++;
			}else if(numero > 0) {
				contadorPositivos++;
				sumaPositivos += numero;
			}else {
				contadorNegativos++;
				sumaNegativos -= numero;
			}
		}
		
		System.out.println("Cantidad de ceros: " + contadorCeros);
		if(contadorPositivos > 0) {
			System.out.println("Media positivos: " + ((double)sumaPositivos/contadorPositivos));
		}
		
		if(contadorNegativos > 0) {
			System.out.println("Media Negativos: " + ((double)sumaNegativos/contadorNegativos));
		}
		
		input.close();
	}

}
