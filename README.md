# Repositorio de programacion curso 19-20
 Los directorios corresponden a los proyectos realizados en eclipse. 
 Dentro del directorio src de cada proyecto accedo a los fuentes de cada ejercicio.

 * ** Ejercicios 01 **: Ejercicios sobre variables, tipos de datos, operadores, casting y clase String
 * ** Ejercicios 02 **: Ejercicios sobre clase String y Scanner
 * ** Ejercicios 03 **: Ejercicios sobre sobre sentencias if-else
 * ** Ejercicios 04 **: Ejercicios sobre sentencia switch
 * ** Ejercicios 05 **: Ejercicios sobre bucles

